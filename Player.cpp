// Methods for the Video Player

#include <vector>
#include <QDebug>

#include "Player.h"
#include "globals.h"

void Player::selection(QUrl path) {
    paused = false;
    setMedia(path);
    play();
}

void Player::toggle() {
    if (paused) {
        play();
        paused = false;
    }
    else {
        pause();
        paused = true;
    }
}

void Player::rewind() {
    setPosition(0);
}
