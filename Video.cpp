// Represents a video and a button to play said video

#include <vector>
#include <string>

#include <QDir>
#include <QString>
#include <QDirIterator>
#include <QFile>
#include <QImageReader>
#include <QImage>
#include <QIcon>
#include <QPixmap>

#include "Video.h"
#include "VideoMetadata.h"
#include "globals.h"

void Video::set(VideoMetadata* m) {
    setIcon(*m->icon);
    setIconSize(QSize(this->width()*1.4, 75));
    metadata = m;
}

// retrieves and initialises video metadata from a given path
std::vector<VideoMetadata> initVideos(char* path) {
    std::vector<VideoMetadata> videos = std::vector<VideoMetadata>();

    QDir directory(QString::fromStdString(std::string(path)));
    QDirIterator iterator(directory);

    while (iterator.hasNext()) {
        QString fileName = iterator.next();
        if (fileName.contains(".")) {
#if defined(_WIN32)
            if (fileName.contains(".wmv"))  { // windows
#else
            if (fileName.contains(".mp4") || fileName.contains(".mov"))  { // mac/linux
#endif
                QString thumbnailPath = fileName.left(fileName.length() - 4) + ".png";
                if (QFile(thumbnailPath).exists()) {
                    QImageReader* imageReader = new QImageReader(
                                                thumbnailPath);
                    QImage thumbnail = imageReader->read();
                    if(!thumbnail.isNull()) {
                        QIcon* icon = new QIcon(
                                    QPixmap::fromImage(thumbnail));
                        QUrl* filePath = new QUrl(
                                    QUrl::fromLocalFile(fileName));
                        QUrl* iconPath = new QUrl(
                                    QUrl::fromLocalFile(thumbnailPath));
                        videos.push_back(
                                    VideoMetadata(
                                        filePath, icon, iconPath));
                    }
                }
            }
        }
    }

    return videos;
}

// initialises videos from a given metadata vector
std::vector<Video*> makeVideos(std::vector<VideoMetadata> vm) {
    std::vector<Video*> videos = std::vector<Video*>();
    Video* current;
    QWidget* orphanage = new QWidget;
    for (int i = 0; i < numVideos; i++) {
        current = new Video(orphanage);
        //current->set(&vm.at(i));
        QUrl pth = QUrl(*vm.at(i).path);
        QUrl iconpth = QUrl(*vm.at(i).iconPath);
        QIcon icn = QIcon(*vm.at(i).icon);
        current->set(new VideoMetadata(&pth, &icn, &iconpth));
        videos.push_back(current);
    }
    return videos;
}

void Video::clicked() {
    emit selection(*metadata->path);
}
