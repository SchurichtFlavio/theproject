// Contains constructor for AudioSlider -  a QWidget which contains the
// equaliser of the video editor

#include <string>

#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QSlider>
#include <QDial>

#include "AudioSlider.h"
#include "globals.h"

AudioSlider::AudioSlider(QSlider* volume) {
    QVBoxLayout* containerLayout = new QVBoxLayout();

    /*
     * H
     * T
     * B
     * Filter
     * Level slider
     * */
    QLabel* lblT = new QLabel();
    lblT->setText("TREBLE");
    lblT->setAlignment(Qt::AlignCenter);
    lblT->setStyleSheet("color: white");

    QLabel* lblM = new QLabel();
    lblM->setText("MID");
    lblM->setAlignment(Qt::AlignCenter);
    lblM->setStyleSheet("color: white");

    QLabel* lblB = new QLabel();
    lblB->setText("BASS");
    lblB->setAlignment(Qt::AlignCenter);
    lblB->setStyleSheet("color: white");

    QLabel* lblF = new QLabel();
    lblF->setText("FILTER");
    lblF->setAlignment(Qt::AlignCenter);
    lblF->setStyleSheet("color: white");

    QLabel* lblV = new QLabel();
    lblV->setText("VOLUME");
    lblV->setAlignment(Qt::AlignCenter);
    lblV->setStyleSheet("color: white");

    QDial *T = new QDial();
    T->setStyleSheet(QString::fromStdString(
                         std::string("background-color: ")
                         + colourThree));
    QDial *M = new QDial();
    M->setStyleSheet(QString::fromStdString(
                         std::string("background-color: ")
                         + colourThree));
    QDial *B = new QDial();
    B->setStyleSheet(QString::fromStdString(
                         std::string("background-color: ")
                         + colourThree));
    QDial *Filter = new QDial();
    Filter->setStyleSheet(QString::fromStdString(
                              std::string("background-color: ")
                              + colourThree));

    volume->setOrientation(Qt::Vertical);

    containerLayout->addWidget(T,12);
    containerLayout->addWidget(lblT,1);

    containerLayout->addWidget(M,12);
    containerLayout->addWidget(lblM,1);

    containerLayout->addWidget(B,12);
    containerLayout->addWidget(lblB,1);

    containerLayout->addWidget(Filter,12);
    containerLayout->addWidget(lblF,1);

    containerLayout->addSpacing(10);
    containerLayout->addWidget(volume,48,Qt::AlignHCenter);
    containerLayout->addWidget(lblV,1,Qt::AlignCenter);

    setLayout(containerLayout);
}
