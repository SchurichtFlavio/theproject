// Storyboards represent the audio waveform and the video timeline

#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QString>
#include <QDebug>

#include "StoryBoard.h"

StoryBoard::StoryBoard(std::string tag) {

    QVBoxLayout* containerLayout = new QVBoxLayout();
    QLabel* previewScreen = new QLabel();

    QFont font = previewScreen->font();
    font.setPointSize(42);

    if (tag.compare("Audio Storyboard") == 0) {
        // set waveform image
        previewScreen->setPixmap(QPixmap(":/img/waveform.png"));
        previewScreen->setScaledContents(true);
        previewScreen->setSizePolicy(QSizePolicy::Ignored,
                                     QSizePolicy::Ignored);
        previewScreen->setMinimumSize(QSize(70*6, 70));
    }
    else {
        // set video timeline
        previewScreen->setPixmap(QPixmap(":/img/timeline.png"));
        previewScreen->setScaledContents(true);
        previewScreen->setSizePolicy(QSizePolicy::Ignored,
                                     QSizePolicy::Ignored);
        previewScreen->setMinimumSize(QSize(70*6, 70));
    }

    containerLayout->addWidget(previewScreen);
    containerLayout->setMargin(0);

    setLayout(containerLayout);
}
