#ifndef GLOBALS_H
#define GLOBALS_H
#include <string>
#include <vector>

#include "Video.h"

// Name of editor
const char title[] = "flaVideo";

const std::string colourOne = "#02111B";
const std::string colourTwo = "#1F2025";
const std::string colourThree = "#5D737E";
const std::string colourFour = "#30292F";

// Current dimensions of window
extern int windowHeight;
extern int windowWidth;

extern int numVideos;

extern std::vector<Video*> pointers;

#endif // GLOBALS_H
