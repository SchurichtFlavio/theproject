// Provides a QWidget which has six different video effects and a
// slider so that the strength of the effect can be altered.

#include <string>

#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QSlider>
#include <QPushButton>

#include "Effects.h"
#include "globals.h"

Effects::Effects() {
    QVBoxLayout* containerLayout = new QVBoxLayout();

    containerLayout->setMargin(0);

    QHBoxLayout* effectButtonsLayout1 = new QHBoxLayout();
    QHBoxLayout* effectButtonsLayout2 = new QHBoxLayout();

    QSlider* effectStrength = new QSlider();
    effectStrength->setOrientation(Qt::Horizontal);

    QPushButton* Reverb = new QPushButton();
    Reverb->setText("Blur");
    Reverb->setStyleSheet(QString::fromStdString(
                              std::string("color: white; "
                                          "background-color: ")
                              + colourThree));

    QPushButton* Echo = new QPushButton();
    Echo->setText("Sharpen");
    Echo->setStyleSheet(QString::fromStdString(
                            std::string("color: white; "
                                        "background-color: ")
                            + colourThree));

    QPushButton* HighPass = new QPushButton();
    HighPass->setText("Contrast");
    HighPass->setStyleSheet(QString::fromStdString(
                                std::string("color: white; "
                                            "background-color: ")
                                + colourThree));

    QPushButton* LowPass = new QPushButton();
    LowPass->setText("Brightness");
    LowPass->setStyleSheet(QString::fromStdString(
                               std::string("color: white; "
                                           "background-color: ")
                               + colourThree));

    QPushButton* Flanger = new QPushButton();
    Flanger->setText("Slow Motion");
    Flanger->setStyleSheet(QString::fromStdString(
                               std::string("color: white; "
                                           "background-color: ")
                               + colourThree));

    QPushButton* Phaser = new QPushButton();
    Phaser->setText("Speed Up");
    Phaser->setStyleSheet(QString::fromStdString(
                              std::string("color: white; "
                                          "background-color: ")
                              + colourThree));

    effectButtonsLayout1->addWidget(Reverb);
    effectButtonsLayout1->addWidget(Echo);
    effectButtonsLayout1->addWidget(HighPass);

    effectButtonsLayout2->addWidget(LowPass);
    effectButtonsLayout2->addWidget(Flanger);
    effectButtonsLayout2->addWidget(Phaser);

    containerLayout->addWidget(effectStrength,33);
    containerLayout->addLayout(effectButtonsLayout1,33);
    containerLayout->addLayout(effectButtonsLayout2,33);

    setLayout(containerLayout);
}
