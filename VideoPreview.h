#ifndef VIDEOPREVIEW_H
#define VIDEOPREVIEW_H
#include <vector>

#include <QWidget>

#include "Video.h"
#include "Player.h"

class VideoPreview : public QWidget {
public:
    VideoPreview(std::vector<VideoMetadata>* vM, Player* videoPlayer);


};
#endif // VIDEOPREVIEW_H
