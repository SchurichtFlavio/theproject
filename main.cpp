// This file shall contain code that is relevant to the main window
// of the project, functionality within the window should be place
// in its respective file.

#include <iostream>
#include <vector>
#include <string>

#include <QApplication>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSize>
#include <QDir>
#include <QDirIterator>
#include <QImageReader>
#include <QMessageBox>

#include <QDebug>

#include "globals.h"
#include "MainWidget.h"
#include "FileContainer.h"
#include "VideoPreview.h"
#include "VideoMetadata.h"
#include "Video.h"
#include "Player.h"
#include "StoryBoard.h"
#include "NavBar.h"
#include "AudioSlider.h"
#include "Effects.h"

int windowHeight;
int windowWidth;
int numVideos;
std::vector<Video*> pointers;

void MainWidget::resizeEvent(QResizeEvent* event) {
    windowWidth = this->width();
    windowHeight = this->height();
}

int main(int argc, char* argv[]) {
    QApplication app(argc, argv);
    MainWidget window;

    if (argc != 2) {
        QMessageBox argError;
        argError.setText("This program should only be run with one"
                         " argument. This is the path to the videos"
                         " folder.");
        argError.setWindowTitle("FlaVideo");
        argError.exec();
        exit(-1);
    }

    // Video object creation
    std::vector<VideoMetadata> videoMeta = initVideos(argv[1]);
    numVideos = videoMeta.size();
    std::vector<Video*> videos = makeVideos(videoMeta);
    QHBoxLayout* fileVideoLayout = new QHBoxLayout();

    if (videos.size() == 0) {
        QMessageBox vidError;
        vidError.setText("No videos found in given path.");
        vidError.setWindowTitle("FlaVideo");
        vidError.exec();
        exit(-1);
    }

    Player* videoPlayer = new Player();
    QSlider* volume = new QSlider();
    StoryBoard* videoStoryBoard = new StoryBoard("Video "
                                                 "Storyboard");
    StoryBoard* audioStoryBoard = new StoryBoard("Audio "
                                                 "Storyboard");
    Effects* effects = new Effects();
    AudioSlider* audioSlider = new AudioSlider(volume);
    NavBar* navBar = new NavBar();

    // connect volume slider to video player volume
    volume->connect(volume, SIGNAL(valueChanged(int)),
                    videoPlayer, SLOT(setVolume(int)));

    FileContainer* fileContainer = new FileContainer(&videoMeta,
                                                     &videos);
    VideoPreview* videoPreview = new VideoPreview(&videoMeta,
                                                  videoPlayer);

    fileVideoLayout->addWidget(fileContainer, 33);
    fileVideoLayout->addWidget(videoPreview, 66);

    // Connect videos to the video player
    for (int i = 0; i < numVideos; i++) {
        videos.at(i)->connect(videos.at(i), SIGNAL(selection(QUrl)),
                              videoPlayer, SLOT(selection(QUrl)));
    }

    // Layout arrangement
    QVBoxLayout* storyBoardLayout = new QVBoxLayout();
    storyBoardLayout->addWidget(videoStoryBoard);
    storyBoardLayout->addWidget(audioStoryBoard);

    QHBoxLayout* effectsStoryBoardLayout = new QHBoxLayout();
    effectsStoryBoardLayout->addWidget(effects, 33);
    effectsStoryBoardLayout->addLayout(storyBoardLayout, 66);

    QVBoxLayout* fileVideoEffectsStoryBoardLayout = new QVBoxLayout();
    fileVideoEffectsStoryBoardLayout->addLayout(fileVideoLayout, 66);
    fileVideoEffectsStoryBoardLayout->addLayout(effectsStoryBoardLayout, 33);

    QHBoxLayout* sliderFileVideoEffectsStoryBoardLayout = new QHBoxLayout();
    sliderFileVideoEffectsStoryBoardLayout->
            addLayout(fileVideoEffectsStoryBoardLayout, 90);
    sliderFileVideoEffectsStoryBoardLayout->addWidget(audioSlider, 10);

    QVBoxLayout* mainLayout = new QVBoxLayout();
    mainLayout->addLayout(sliderFileVideoEffectsStoryBoardLayout, 90);
    mainLayout->setMargin(50);
    mainLayout->setSpacing(20);

    QVBoxLayout* mainLayoutNavBar = new QVBoxLayout();
    mainLayoutNavBar->addWidget(navBar);
    mainLayoutNavBar->addLayout(mainLayout);
    mainLayoutNavBar->setMargin(0);

    // Window setup
    window.setLayout(mainLayoutNavBar);
    window.setWindowTitle(title);
    window.setWindowState(Qt::WindowMaximized);
    window.setStyleSheet(QString::fromStdString(
                             std::string("background-color: ")
                             + colourOne));
    window.show();

    return app.exec();
}
