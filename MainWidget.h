#ifndef MAINWIDGET_H
#define MAINWIDGET_H
#include <QWidget>

class MainWidget : public QWidget {
protected:
    void resizeEvent(QResizeEvent* event);
};

#endif // MAINWIDGET_H
