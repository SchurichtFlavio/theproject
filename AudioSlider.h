#ifndef AUDIOSLIDER_H
#define AUDIOSLIDER_H
#include <QWidget>
#include <QSlider>

class AudioSlider : public QWidget {
public:
    AudioSlider(QSlider* volume);
};

#endif // AUDIOSLIDER_H
