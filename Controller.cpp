// Constructor for the Controller of the VideoPlayer

#include <QWidget>
#include <QStyle>
#include <QHBoxLayout>

#include "Controller.h"

Controller::Controller(QWidget* parent) : QWidget(parent) {
    play = new QToolButton(this);
    play->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));

    stop = new QToolButton(this);
    stop->setIcon(style()->standardIcon(QStyle::SP_MediaStop));

    rwd = new QToolButton(this);
    rwd->setIcon(style()->standardIcon(QStyle::SP_MediaSkipBackward));

    fwd = new QToolButton(this);
    fwd->setIcon(style()->standardIcon(QStyle::SP_MediaSkipForward));

    QHBoxLayout* layout = new QHBoxLayout();

    layout->setMargin(0);
    layout->addWidget(rwd);
    layout->addWidget(play);
    layout->addWidget(stop);
    layout->addWidget(fwd);
    setLayout(layout);
}
