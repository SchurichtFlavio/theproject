QT += core gui widgets multimedia multimediawidgets

CONFIG += c++11


# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        AudioSlider.cpp \
        Controller.cpp \
        Effects.cpp \
        FileContainer.cpp \
        NavBar.cpp \
        Player.cpp \
        StoryBoard.cpp \
        Video.cpp \
        VideoPreview.cpp \
        main.cpp

HEADERS += \
    AudioSlider.h \
    Controller.h \
    DynamicLayout.h \
    Effects.h \
    FileContainer.h \
    MainWidget.h \
    NavBar.h \
    Player.h \
    StoryBoard.h \
    Video.h \
    VideoMetadata.h \
    VideoPreview.h \
    globals.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES +=

RESOURCES += \
    Resources.qrc

