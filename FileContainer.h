#ifndef FILECONTAINER_H
#define FILECONTAINER_H
#include <vector>

#include <QWidget>
#include <QSize>

#include "Video.h"

class FileContainer : public QWidget {
public:
    FileContainer(std::vector<VideoMetadata>* videoMeta,
                  std::vector<Video*>* videos);
};

#endif // FILECONTAINER_H
