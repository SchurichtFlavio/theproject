#ifndef CONTROLLER_H
#define CONTROLLER_H
#include <QWidget>
#include <QToolButton>

class Controller : public QWidget {
Q_OBJECT
public:
    Controller(QWidget* parent);

    QToolButton* play;
    QToolButton* stop;
    QToolButton* rwd;
    QToolButton* fwd;
};

#endif // CONTROLLER_H
