#ifndef VIDEO_H
#define VIDEO_H
#include <QPushButton>
#include <QWidget>
#include <QSize>
#include <QPaintEvent>

#include "VideoMetadata.h"

class Video : public QPushButton {
Q_OBJECT
public:
    VideoMetadata* metadata;
    Video() {
        setIconSize(QSize(200, 110));
    }

    Video(QWidget* parent) : QPushButton(parent) {
        setIconSize(QSize(200, 110));
        connect(this, SIGNAL(released()), this, SLOT (clicked() ));
    }

    void set(VideoMetadata* m);
private slots:
    void clicked();
signals:
    void selection(QUrl);
};

std::vector<VideoMetadata> initVideos(char* path);
std::vector<Video*> makeVideos(std::vector<VideoMetadata> vm);

#endif // VIDEO_H
