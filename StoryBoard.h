#ifndef STORYBOARD_H
#define STORYBOARD_H
#include <QWidget>

class StoryBoard : public QWidget {
public:
    StoryBoard(std::string tag);
};
#endif // STORYBOARD_H
