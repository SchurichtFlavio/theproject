// The widget contains the video player and its controller

#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QVideoWidget>
#include <QMediaPlayer>
#include <QDebug>

#include "globals.h"
#include "VideoPreview.h"
#include "Player.h"
#include "Controller.h"

VideoPreview::VideoPreview(std::vector<VideoMetadata>* vM,
                           Player* videoPlayer) {
    QVBoxLayout* containerLayout = new QVBoxLayout();

    QVideoWidget* videoHandler = new QVideoWidget();
    videoPlayer->setVideoOutput(videoHandler);
    videoPlayer->selection(*vM->at(0).path);

    videoHandler->setStyleSheet("background-color: black");

    Controller* controls = new Controller(this);

    connect(controls->play, SIGNAL(clicked()),
            videoPlayer, SLOT(toggle()));
    connect(controls->stop, SIGNAL(clicked()),
            videoPlayer, SLOT(stop()));
    connect(controls->rwd, SIGNAL(clicked()),
            videoPlayer, SLOT(rewind()));
    connect(controls->fwd, SIGNAL(clicked()),
            videoPlayer, SLOT(stop()));

    containerLayout->addWidget(videoHandler);
    containerLayout->addWidget(controls);
    containerLayout->setMargin(0);

    setLayout(containerLayout);
}
