#ifndef PLAYER_H
#define PLAYER_H
#include <vector>

#include <QMediaPlayer>

#include "Video.h"

class Player : public QMediaPlayer {
Q_OBJECT
private:
    //std::vector<VideoMetadata>* data;
    //std::vector<Video*>* videos;
    bool paused;
public:
    Player() : QMediaPlayer(NULL) {
        setVolume(0);
    }

    //void init(std::vector<VideoMetadata>* d, std::vector<Video*>* p);

public slots:
    void selection(QUrl path);
    void toggle();
    void rewind();
};

#endif // PLAYER_H
