// Contains the videos that were found by the video editor

#include <vector>
#include <string>

#include <QWidget>
#include <QLabel>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSize>
#include <QScrollArea>

#include "globals.h"
#include "FileContainer.h"
#include "Video.h"
#include "VideoMetadata.h"
#include "VideoPreview.h"

std::vector<Video*> ptrs;


void setVideoVector(std::vector<Video*> v) {
    ptrs = std::vector<Video*>(v);
}

std::vector<Video*> getVideoVector() {
    return ptrs;
}

FileContainer::FileContainer(std::vector<VideoMetadata>* videoMeta,
                             std::vector<Video*>* videos) {
    QVBoxLayout* containerLayout = new QVBoxLayout();

    // Video selection pane
    QWidget* videoWidget = new QWidget();
    QGridLayout* videoGrid = new QGridLayout(videoWidget);
    Video* video;

    // Add each video into grid layout
    for (int i = 0; i < numVideos; i++) {
        video = videos->at(i);
        video->setParent(this);
        videoGrid->addWidget(video, i / 2, i % 2);
        video->set(&videoMeta->at(i));
    }

    //videoWidget->setFixedSize(QSize(this->width(), this->height()));
    videoGrid->setMargin(10);
    videoGrid->setSpacing(10);
    videoGrid->setAlignment(Qt::AlignTop);

    videoWidget->setObjectName("vW");
    videoWidget->setStyleSheet(QString::fromStdString(
                                   std::string(
                                       "QWidget#vW{ background-color: "
                                   )
                                   + colourTwo + std::string("; }")));
    videoWidget->setLayout(videoGrid);

    containerLayout->addWidget(videoWidget);
    containerLayout->setMargin(0);
    containerLayout->setSpacing(0);

    setLayout(containerLayout);
}

