// Implementation of a navigation bar at the top of the window

#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QMenuBar>

#include "NavBar.h"
#include "globals.h"

NavBar::NavBar() {

    QVBoxLayout* containerLayout = new QVBoxLayout();
    containerLayout->setMargin(0);

    QString colour = QString::fromStdString(std::string(
                                                "color:white;"
                                                "background-color: ")
                                            + colourFour
                                            + std::string(";"));

    //Menu Bar
    QMenu *file = new QMenu("File");
    file->setStyleSheet(colour);
    file->addAction("New");
    file->addAction("Open");
    file->addAction("Save");


    QMenu *format = new QMenu("View");
    format->setStyleSheet(colour);
    format->addAction("Dark Mode");
    format->addAction("Light Mode");


    QMenu *edit = new QMenu("Edit");
    edit->setStyleSheet(colour);
    edit->addAction("Undo");
    edit->addAction("Redo");
    edit->addAction("Cut");
    edit->addAction("Copy");
    edit->addAction("Paste");

    QMenu *settings = new QMenu("Settings");
    settings->setStyleSheet(colour);
    settings->addAction("Preferences");
    settings->addMenu(format);

    QMenu *about = new QMenu("About");
    about->setStyleSheet(colour);
    about->addAction("FlaVideo");


    QMenuBar *menu = new QMenuBar();
    menu->setStyleSheet(colour);
    menu->addMenu(file);
    menu->addMenu(edit);
    menu->addAction("Effect");
    menu->addMenu(settings);
    menu->addAction("Help");
    menu->addMenu(about);

    containerLayout->addWidget(menu);
    setLayout(containerLayout);
}
