#ifndef VIDEOMETADATA_H
#define VIDEOMETADATA_H
#include <QUrl>
#include <QIcon>

class VideoMetadata {
public:
    QUrl* path;
    QIcon* icon;
    QUrl* iconPath;
    VideoMetadata(QUrl* path, QIcon* icon, QUrl* iconPath)
        : path(path), icon(icon), iconPath(iconPath) {}
};

#endif // VIDEOMETADATA_H
